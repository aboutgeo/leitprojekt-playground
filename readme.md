# DME Leitprojekt 2013 - Playground

Just some trial/utility code I'm producing as part of this year's 'Leitprojekt'. Code is written in
[Scala](http://www.scala-lang.org/) and uses [SBT](http://www.scala-sbt.org/) as a build tool. Please
refer to the [SBT docs](http://www.scala-sbt.org/release/docs/index.html) for instructions on how to
install SBT on your machine.

__NOTE: the graph drawing code has moved to a [separate project on GitHub](https://github.com/rsimon/scala-force-layout)!__

Once you have installed SBT:

* Type ``sbt run`` to start.

* To generate an Eclipse project, type ``sbt eclipse``.

## Experiment 1: Term Co-Occurence in HTML 

This little utility generates a co-occurence graph of the N most frequent terms mentioned
in a collection of HTML pages. The sample pages are located in ``src/test/resources``. The
graph is shown in a window with mouse zoom and pan functionality.

After start, select the ``HTMLCoOccurenceGraph`` application to run.

