name := "leitprojekt-playground"

version := "0.1"

scalaVersion := "2.10.1"

resolvers ++= Seq(
  "Internet Archive Maven Repository" at "http://builds.archive.org:8080/maven2"
)

libraryDependencies ++= Seq(
  "org.jsoup" % "jsoup" % "1.7.2",
  "net.sf.jung" % "jung-graph-impl" % "2.0.1",
  "net.sf.jung" % "jung-visualization" % "2.0.1",
  "com.propensive" % "rapture-io" % "0.7.2",
  "org.jwat" % "jwat-warc" % "1.0.0"
)
