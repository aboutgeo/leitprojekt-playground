package at.ait.dme.leitprojekt2013.webarchives

import scala.collection.JavaConversions._
import org.jsoup.Jsoup
import scala.collection.IndexedSeq
import org.jsoup.nodes.Element

/**
 * Utility functions for extracting basic metadata from raw HTML.
 * @author Rainer Simon <rainer.simon@ait.ac.at>
 */
class HTMLMetadata(html:String) {

  private lazy val el = Jsoup.parse(html)
  
  private val stopwords_en = IndexedSeq(
    "a", "all", "an", "also", "and", "are", "as", "at", "be", "been", "but", "by", "can", "do", "e", "for", "from", "has",
    "have", "i", "in", "into", "is", "it", "more", "no", "not", "of", "on", "or", "other", "our", "s", "so", "such", "that", "the", "their",
    "them", "there", "these", "this", "to", "via", "was", "we", "when", "which", "with", "yes", "you", "your"
  )
  
  private val stopwords_de = IndexedSeq(
    "antworten", "derstandard", "mal", "ihnen", "geht", "selbst", "melden", "aber", "alles", "als", "am", "an", "at", "auch", "auf", "aus", "bei", "bis", "bitte", "da", "dann", "das", "dass", "daß", "dem", "den", "der",
    "des", "die", "diese", "durch", "ein", "eine", "einem", "einen", "einer", "eh", "er", "es", "etwas", "fuer", "für", "ganz", "gibt", "hab", "habe", "haben", 
    "hat", "hätte", "hier", "ich", "ihre", "in", "im", "ist", "ja", "jetzt", "kann", "kein", "keine", "können", "man", "mehr", "meiner", "mich", "mir", "mit", "nach", 
    "nicht", "noch", "nur", "oder", "oft", "permalink", "schon", "seit", "sehr", "sich", "sie", "sind", "so", "soll", "um", "und", "über", "von", "war", "was", "wenn", "werden",
    "wie", "will", "wir", "wird", "wurde", "zu", "zum", "zur", "muss", "mein", "damit", "ihr"
  )
  
  private val stopwords = stopwords_de ++ stopwords_en
  
  private val delimiters = Seq(
    "\\.", ",", ":", ";", "\\(", "\\)", "<", ">", "!", "\\?", "\"", "“", "'", "/"
  )
  
  private lazy val tokenizedText = {
    val regex = "\\s*(\\s|" + delimiters.mkString("|") + ")\\s*"
    try {
      el.body.text.split(regex).map(_.toLowerCase).filter(token => {
         token.length > 0 && 
         token.forall(_.isLetterOrDigit) && 
         !token.forall(_.isDigit)
      }).toSeq
    } catch {
      case t: Throwable => Seq.empty[String]
    }
  }
  
  def wordFrequency(n: Int = 5) = 
    tokenizedText.filter(!stopwords.contains(_)).groupBy(w => w).mapValues(_.size).toList.sortBy(_._2).takeRight(n).reverse
    
  lazy val links = el.getElementsByTag("a").iterator().map(el => el.attr("href")).toSet

}
