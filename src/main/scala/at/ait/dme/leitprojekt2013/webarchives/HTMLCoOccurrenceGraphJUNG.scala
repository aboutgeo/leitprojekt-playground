package at.ait.dme.leitprojekt2013.webarchives

import edu.uci.ics.jung.graph.SparseMultigraph
import edu.uci.ics.jung.algorithms.layout.Layout
import java.awt.Dimension
import edu.uci.ics.jung.visualization.BasicVisualizationServer
import javax.swing.JFrame
import edu.uci.ics.jung.algorithms.layout.SpringLayout2
import java.io.File
import scala.io.Source
import edu.uci.ics.jung.algorithms.layout.FRLayout
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller
import org.apache.commons.collections15.Transformer
import edu.uci.ics.jung.visualization.renderers.VertexLabelRenderer
import edu.uci.ics.jung.visualization.renderers.DefaultVertexLabelRenderer
import java.awt.Color
import java.awt.Font
import java.awt.Stroke
import java.awt.BasicStroke
import java.awt.Paint
import java.awt.event.ComponentAdapter
import java.awt.event.ComponentEvent
import edu.uci.ics.jung.visualization.control.EditingModalGraphMouse
import edu.uci.ics.jung.visualization.VisualizationViewer
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse
import edu.uci.ics.jung.visualization.control.ModalGraphMouse

/**
 * @author Rainer Simon <rainer.simon@ait.ac.at>
 */
object HTMLCoOccurrenceGraphJUNG extends App {
  
  // Parse input files
  println("Parsing WARCs...")
  val meta = WARCExtractor.extractHTML("src/test/resources/sample-large.warc.gz").map(html => new HTMLMetadata(html).wordFrequency(10)).filter(_.size > 1).take(1000)    
  println("Got " + meta.size + " HTML files")

  // Compute co-occurence matrix
  println("Computing term co-occurrence matrix")
  val matrix = CoOccurenceMatrix.build(meta.map(_.map(_._1)))
  
  // Draw graph
  val allTerms = meta.flatten.groupBy(_._1).map(tuple => (tuple._1, tuple._2.foldLeft(0)((a, b) => a + b._2))).toSeq
  println(allTerms.size + " terms (= graph nodes)")
  println(matrix.size + " co-occurrences (= edges)")
  
  println("Drawing graph")  
  val startTime = System.currentTimeMillis
  val graph = new SparseMultigraph[String, String]
  allTerms.foreach { case (term, count) => graph.addVertex(term) }  
  matrix.zipWithIndex.map { case ((pair, count), idx) => {
    val keywords = pair.toArray
    graph.addEdge("edge_" + idx, keywords(0), keywords(1))
  }}

  val layout = new FRLayout(graph)
  val dim = (new Dimension(920, 740))
  layout.setSize(dim)
  
  val vis = new VisualizationViewer(layout)
  vis.setPreferredSize(dim)  
  vis.getRenderContext().setVertexLabelTransformer(new ToStringLabeller())
  vis.getRenderContext().setVertexFontTransformer(new Transformer[String, Font]() {
    val sizes = allTerms.toMap
    def transform(label: String): Font = {
      val size = 9 + 4 * Math.log(sizes.get(label).getOrElse(1).toDouble)
      new Font("Verdana", Font.PLAIN, size.toInt)
    }
  })
  vis.getRenderContext().setEdgeDrawPaintTransformer(new Transformer[String, Paint]() {
    def transform(label: String): Paint = new Color(180, 180, 180)
  })
  val mouse = new DefaultModalGraphMouse
  mouse.setMode(ModalGraphMouse.Mode.TRANSFORMING)
  vis.setGraphMouse(mouse)
  
  val frame = new JFrame("Keyword Co-Occurence in HTML Corpus")
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
  frame.getContentPane().add(vis) 
  frame.pack()
  frame.setVisible(true)
  
  println("Done. Took " + (System.currentTimeMillis - startTime) + "ms.")
  
}
