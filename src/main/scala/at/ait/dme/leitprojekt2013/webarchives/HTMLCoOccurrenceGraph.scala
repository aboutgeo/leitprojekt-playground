package at.ait.dme.leitprojekt2013.webarchives

import javax.swing.JFrame
import javax.swing.ImageIcon
import java.awt.Dimension
import javax.swing.JLabel
import java.awt.image.BufferedImage
import java.awt.Graphics2D
import java.awt.RenderingHints
import java.awt.Color
import java.awt.geom.Line2D
import java.awt.geom.Ellipse2D
import java.awt.Font
import java.awt.BasicStroke
import at.ait.dme.forcelayout.Node
import at.ait.dme.forcelayout.SpringGraph
import at.ait.dme.forcelayout.Edge
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import at.ait.dme.forcelayout.renderer.InteractiveGraphRenderer

object HTMLCoOccurrenceGraph extends App {
  
  // Parse input files
  println("Parsing WARCs...")
  val startTime = System.currentTimeMillis
  val meta = 
    WARCExtractor.extractHTML("src/test/resources/sample-large.warc.gz").map(html => new HTMLMetadata(html).wordFrequency(10)).filter(_.size > 1).take(1000)
  println("Got " + meta.size + " HTML files. Took " + (System.currentTimeMillis - startTime) + " ms")

  // Compute co-occurence matrix
  println("Computing term co-occurrence matrix")
  val matrix = CoOccurenceMatrix.build(meta.seq.map(_.map(_._1)))
  println("Took " + (System.currentTimeMillis - startTime) + " ms")
  
  // Draw graph
  println("Drawing graph")
  val allTerms = meta.flatten.groupBy(_._1).map(tuple => (tuple._1, tuple._2.foldLeft(0)((a, b) => a + b._2)))
  val maxValue = Math.log10(allTerms.maxBy(_._2)._2.toDouble)
  
  val vertices = allTerms.seq.toSeq.map(tuple => (tuple._1, tuple._1))
  val links = matrix.toSeq.map(pair => {
        val terms = pair._1.toArray
        (terms(0), terms(1)) 
      })
  
  val nodes = vertices.map(vertex => Node(vertex._1, vertex._2, 0.2))
  
  val edges = links.map(edge => {
    val from = nodes.find(_.id.equals(edge._1))
    val to = nodes.find(_.id.equals(edge._2))
    
    if (from.isDefined && to.isDefined)
      Edge(from.get, to.get)
    else
      null
  }).filter(_ != null)
      
  val graph = new SpringGraph(nodes, edges) 
  
  val vis = new InteractiveGraphRenderer(graph)
  
  val frame = new JFrame("Term Co-Occurence: " + meta.size + " HTML pages, " + allTerms.size + " Terms")
  frame.setSize(920, 720)
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
  frame.getContentPane().add(vis) 
  frame.pack()
  frame.setVisible(true)
  
  vis.start

}