package at.ait.dme.leitprojekt2013.webarchives

import scala.io.Source
import at.ait.dme.forcelayout.Edge
import at.ait.dme.forcelayout.Node
import at.ait.dme.forcelayout.SpringGraph
import at.ait.dme.forcelayout.renderer.BufferedInteractiveGraphRenderer
import javax.swing.JFrame
import java.awt.Dimension

object BLLinkGraph extends App {
  
  val YEAR = "2010"
 
  val nodes = readData.filter(_(0).equals(YEAR)).map(record => {
    val from = record(1)
    val to = record(2).split("\t")(0)
    Seq(Node(from, from), Node(to, to))  
  }).flatten.toSet.toIndexedSeq
  
  val edges = readData.filter(_(0).equals(YEAR)).map(record => {
    val from = nodes.find(node => node.id.equals(record(1)))
    val to = nodes.find(node => node.id.equals(record(2).split("\t")(0)))
    val size = record(2).split("\t")(1).toInt
    if (from.isDefined && to.isDefined)
      Edge(from.get, to.get, size)
    else
      null
  }).filter(_ != null).toSeq
  
  val graph = new SpringGraph(nodes, edges)
  
  val vis = new BufferedInteractiveGraphRenderer(graph)
  
  val frame = new JFrame("BL Link Graph 1996 - " + nodes.size + " Nodes, " + edges.size + " Edges")
  frame.setPreferredSize(new Dimension(920, 720))
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
  frame.getContentPane().add(vis) 
  frame.pack()
  frame.setVisible(true)
  
  vis.start

  def readData = Source.fromFile("src/test/resources/bl-uk-linkage.tsv").getLines.map(_.split("\\|"))
    
}