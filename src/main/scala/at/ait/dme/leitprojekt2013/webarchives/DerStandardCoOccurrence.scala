package at.ait.dme.leitprojekt2013.webarchives

import scala.io.Source
import at.ait.dme.forcelayout.renderer.InteractiveGraphRenderer
import javax.swing.JFrame
import java.awt.Dimension
import at.ait.dme.forcelayout.Node
import at.ait.dme.forcelayout.Edge
import at.ait.dme.forcelayout.SpringGraph
import at.ait.dme.forcelayout.renderer.BufferedInteractiveGraphRenderer

object DerStandardCoOccurrence extends App {
  
  val startTime = System.currentTimeMillis

  // Read data
  print("Reading data... ")
  val topTerms = Source.fromFile("src/test/resources/standard-topterms.txt").getLines.take(50000).map(line => {
    line.split("\\t")(1).split("\\|").map(term => {
      val pair = term.split(",")
      (pair(0), pair(1).toInt)
    }).toSeq
  }).toSeq

  // Collapse all terms
  val allTerms = topTerms.flatten.groupBy(_._1).mapValues(seq => seq.foldLeft(0)(_ + _._2))
  println("got " + allTerms.size + " unique terms in " + topTerms.size + " HTML pages")
  
  // Build co-occurrence matrix
  print("Building co-occurrence matrix... ")  
  val matrix = CoOccurenceMatrix.build(topTerms.map(seq => seq.map(_._1)))
  println("took " + (System.currentTimeMillis - startTime) + " ms.")
  
  // Draw graph
  print("Computing graph... ")
  val nodes = allTerms.map(tuple => tuple._1 -> Node(tuple._1, tuple._1, tuple._2))
  print(nodes.size + " nodes... ")
  val edges = matrix.toSeq.map(pair => {
    val terms = pair._1.toArray
    val from = nodes.get(terms(0))
    val to = nodes.get(terms(1))
    
    if (from.isDefined && to.isDefined)
      Edge(from.get, to.get, pair._2)
    else
      null
  }).filter(_ != null)
  println(edges.size + " edges.")
  
  println("Drawing.")      
  val graph = new SpringGraph(nodes.values.toSeq, edges) 

  val vis = new BufferedInteractiveGraphRenderer(graph)
  
  val frame = new JFrame("Co-Occurrence")
  frame.setPreferredSize(new Dimension(920, 720))
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
  frame.getContentPane().add(vis) 
  frame.pack()
  frame.setVisible(true)
  
  vis.start
  println("Took " + (System.currentTimeMillis - startTime) + "ms")
  
}