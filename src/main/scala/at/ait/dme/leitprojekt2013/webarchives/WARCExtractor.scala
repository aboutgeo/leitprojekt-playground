package at.ait.dme.leitprojekt2013.webarchives

import scala.collection.JavaConversions._
import scala.io.Source
import org.jwat.warc.WarcReaderFactory
import java.io.FileInputStream

object WARCExtractor {
  
  private val TEXT_HTML = "text/html"
    
  private val UTF8 = "UTF-8"
    
  private val NEWLINE = "\n"
    
  def extractHTML(filepath: String): Seq[String] =
    WarcReaderFactory.getReader(new FileInputStream(filepath)).iterator
      // filter HTTP 200 responses
      .filter(record => record.getHttpHeader != null && record.getHttpHeader.statusCode == 200)
      // filter HTML content types only
      .filter(record => record.getHttpHeader.contentType.startsWith(TEXT_HTML))
      // map record iterator to HTML payload iterator
      .map(record => {
        val contentType = record.getHttpHeader.contentType
        val charsetIdx = contentType.indexOf("; charset=")
        val encoding = if (charsetIdx > -1)
            contentType.substring(charsetIdx + 10)
          else
            UTF8
        try {
          Some(Source.fromInputStream(record.getPayloadContent)(io.Codec(encoding)).getLines().mkString(NEWLINE))
        } catch {
          case t: Throwable => { 
            println("[WARNING] Error parsing record (encoding: " + encoding + ")")
            None
          }
        }
      }).filter(html => html.isDefined).map(_.get).toSeq
  

}