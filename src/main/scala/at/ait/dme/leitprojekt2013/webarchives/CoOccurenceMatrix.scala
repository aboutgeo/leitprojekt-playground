package at.ait.dme.leitprojekt2013.webarchives

import scala.annotation.tailrec
import scala.collection.immutable.HashMap

/**
 * Contains a single public function which builds a co-occurence matrix from a
 * a collection of term-lists. (I.e. co-occurence is when two terms occur in the
 * same term-list.)
 * @author Rainer Simon <rainer.simon@ait.ac.at>
 */
object CoOccurenceMatrix {
  
  /**
   * Builds the co-occurence matrix, by picking the first term-list in the Seq, 
   * and then recursing with the tail.
   */
  def build(terms: Seq[Seq[String]], matrix: Map[Set[String], Int] = HashMap.empty[Set[String], Int]): Map[Set[String], Int] = {
    val combinations = computeCombinations(terms.head)
    val filledMatrix = fillMatrix(combinations, matrix)
    val tail = terms.tail;
    if (tail.isEmpty)
      filledMatrix
    else
      build(tail, filledMatrix)
  }
  
  /**
   * Another helper function that fills the matrix for a given set of co-occurences.
   */
  private def fillMatrix(wordpairs: Set[Set[String]], matrix: Map[Set[String], Int]): Map[Set[String], Int] = {      
    val pair = wordpairs.head
    val count = matrix.get(pair)
    val filledMatrix = if (count.isDefined)
	  matrix + (pair -> (count.get + 1))	
	else
	  matrix + (pair -> 1)	
	  
	val tail = wordpairs.tail
	if (tail.isEmpty)
	  filledMatrix
	else
	  fillMatrix(tail, filledMatrix)
  }
  
  /**
   * A helper function that computes all possible pair-wise combinations of terms in a list.
   */
  private def computeCombinations(wordlist: Seq[String]) = {
    wordlist.map(wordA => {
      wordlist.map(wordB => { Set(wordA, wordB) }).filter(pair => pair.size == 2)
    }).flatten.toSet
  }  
  
}
