package at.ait.dme.leitprojekt2013.pelagios

import scala.io.Source
import at.ait.dme.forcelayout.{ Edge, Node, SpringGraph }
import javax.swing.JFrame
import java.awt.Dimension
import rapture.io._
import at.ait.dme.forcelayout.renderer.BufferedInteractiveGraphRenderer
import at.ait.dme.forcelayout.renderer.OpenGLInteractiveGraphRenderer
import at.ait.dme.forcelayout.renderer.InteractiveGraphRenderer
import at.ait.dme.forcelayout.renderer.Node2D
import java.awt.Graphics2D
import java.awt.geom.Ellipse2D
import java.awt.Color
import java.awt.BasicStroke
import at.ait.dme.forcelayout.renderer.ImageRenderer
import at.ait.dme.forcelayout.renderer.ColorPalette
import javax.imageio.ImageIO
import java.io.File

object PlacesInTheAncientWorld extends App {
  
  val STATIC = true

  println("Loading data")
  val json = Json.parse(Source.fromFile("src/test/resources/pelagios-data.json").mkString)
  
  val nodes: Map[Int, Node] = (json.nodes).get[List[Json]].map(json => {
      val name = json.name.get[String].toString
      val index = json.index.get[Int]
      index -> Node(index.toString, name)
    }).toMap
    
  val edges = (json.links).get[List[Json]].map(json => {
    Edge(nodes(json.source.get[Int]), nodes(json.target.get[Int]))
  })
    
  println(nodes.size + " nodes")
  println(edges.size + " edges")

  val startTime = System.currentTimeMillis
  val graph = new SpringGraph(nodes.values.toSeq, edges) 
 
  println("Took " + (System.currentTimeMillis - startTime) + "ms")

  println("Drawing...")
  val nodePainter = (nodes: Seq[Node2D], g2d: Graphics2D) => {
    nodes.foreach(n2d => {
      val (x, y, n) = (n2d.x, n2d.y, n2d.node)
      val size = 1 + 4 * Math.log10(n.links.size.toDouble)
      val color = ColorPalette.getColor(n.group) 
      g2d.setColor(color)
      g2d.fill(new Ellipse2D.Double(x - size / 2, y - size / 2, size, size))
      g2d.setStroke(new BasicStroke(1));
      g2d.setColor(color.darker)
      g2d.draw(new Ellipse2D.Double(x - size / 2, y - size / 2, size, size))
    }) 
    
    nodes.filter(_.node.links.size > 14).foreach(n2d => {
       g2d.setColor(Color.BLACK)
       g2d.drawString(n2d.node.label, n2d.x + 5, n2d.y - 2)     
    })
  }
  
  if (STATIC) {
    graph.doLayout(onComplete = (it => {
        val image = ImageRenderer.drawGraph(graph, 4000, 4000)
        ImageIO.write(image, "png", new File("places-in-the-ancient-world.png"))
      }))
  } else {
    val vis = new BufferedInteractiveGraphRenderer(graph) 
    vis.setNodePainter(nodePainter)
    val frame = new JFrame("Places in the Ancient World")
    frame.setPreferredSize(new Dimension(920, 720))
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    frame.getContentPane().add(vis) 
    frame.pack()
    frame.setVisible(true)
    vis.start
  }
  
  println("Done")

}