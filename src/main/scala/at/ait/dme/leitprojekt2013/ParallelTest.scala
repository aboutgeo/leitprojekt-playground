package at.ait.dme.leitprojekt2013

case class Call(i: Int) { 
  def call { Thread.sleep(100) } 
}

object ParallelTest extends App {

  println("starting")
  
  val start0 = System.currentTimeMillis
  (1 to 1000000).toArray.map(_*2)
  println(System.currentTimeMillis - start0)
  
  val start1 = System.currentTimeMillis
  (1 to 1000000).toArray.par.map(_*2)
  println(System.currentTimeMillis - start1)
  
  val start2 = System.currentTimeMillis
  val arr = (1 to 100).toArray.map(new Call(_))
  arr.map(_.call)
  println(System.currentTimeMillis - start2)

  val start3 = System.currentTimeMillis
  val parr = arr.par
  parr.map(_.call)
  println(System.currentTimeMillis - start3)

}